# Specifications
# 1. Create a Person class that is an abstract class that has the following abstractclassmethods
#   a. getFullName method
#   b. addRequest method
#   c. checkRequest method
#   d. addUser method

from abc import ABC, abstractclassmethod

class Person(ABC):

    @abstractclassmethod
    def getFullName(self):
        pass

    @abstractclassmethod
    def addRequest(self):
        pass

    @abstractclassmethod
    def checkRequest(self):
        pass

    @abstractclassmethod
    def addUser(self):
        pass

# 2. Create an Employee class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department
#   b. Methods (Don't forget getters and setters)
#   c. Abstract methods
#       i. checkRequest() - placeholder method (For the checkRequest and addUser methods, they should do nothing, for the functions that does nothing, 'pass' keyword will be a great help)
#       ii. addUser  - placeholder method (For the checkRequest and addUser methods, they should do nothing, for the functions that does nothing, 'pass' keyword will be a great help)
#       iii. addRequest - returns "Request has been added"
#       iv. getFullName - returns both object's firstname and lastname through self
#   d. Custom Methods
#       v. login() - returns "<Email> has logged in"
#       vi. logout() - returns "<Email> has logged out"

class Employee(Person):

    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def setFirstName(self,firstName):
        self._firstName = firstName

    def setLastName(self, lastName):
        self._lastName = lastName

    def setEmail(self, email):
        self._email = email

    def setDepartment(self, department):
        self._email = email

    def getInfo(self):
        print(f'My name is {self._fistName} {self._lastName}. My email is {self._email}. I am working in {self._department} department.')

    def checkRequest(self):
        pass

    def addUser(self):
        pass

    def addRequest(self):
        return('Request has been added')

    def getFullName(self):
        return(f"{self._firstName} {self._lastName}")

    def login(self):
        return(f'{self._email} has logged in')

    def logout(self):
        return(f'{self._email} has logged out')



# 3. Create a TeamLead class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department, members 
#       Note: members could be a list so it will allow values
#   b. Methods (Don't forget getters and setters)
#       Abstract methods
#       i. addRequest() - For the addRequest() and addUser() methods, they should do nothing. For the functions that do nothing, 'pass' keyword will be a great help)
    #   ii. addUser() - For the addRequest() and addUser() methods, they should do nothing. For the functions that do nothing, 'pass' keyword will be a great help)
#       iii. checkRequest - returns "Request has been checked"
#       iv. getFullName - returns both object's firstname and lastname through self
#   d. Custom Methods
#       v. login() - returns "<Email> has logged in"
#       vi. logout() - returns "<Email> has logged out"
#       Vii. addMember() - adds an employee to the members list

class TeamLead(Person):

    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._members = []

    def setFirstName(self,firstName):
        self._firstName = firstName

    def setLastName(self, lastName):
        self._lastName = lastName

    def setEmail(self, email):
        self._email = email

    def setDepartment(self, department):
        self._email = email

    def setMembers(self, members):
        self._members = members[len[members]]

    def getInfo(self):
        print(f'My name is {self._fistName} {self._lastName}. My email is {self._email}. I am working in {self._department} department.')

    def checkRequest(self):
        return('Request has been checked')

    def addUser(self):
        pass

    def addRequest(self):
        pass

    def getFullName(self):
        return(f"{self._firstName} {self._lastName}")

    def login(self):
        return(f'{self._email} has logged in')

    def logout(self):
        return(f'{self._email} has logged out')

    def addMember(self, members):
        self._members.append(members)

    def get_members(self):
        return(self._members)



# 4. Create an Admin class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department
#   b. Methods
#       Abstract methods
#       i. checkRequest() - placeholder method (For the checkRequest() and addRequest() methods, they should do nothing, for the functions that does nothing, 'pass' keyword will be a great help)
#       ii. addRequest() - placeholder method (For the checkRequest() and addRequest() methods, they should do nothing, for the functions that does nothing, 'pass' keyword will be a great help)
#       iii. addUser() - outputs "New user added"
     #  iv. getFullName - returns both object's firstname and lastname through self
#   d. Custom Methods
#       v. login() - outputs "<Email> has logged in"
#       vi. logout() - outputs "<Email> has logged out"
#       Note: All methods just return Strings of simple text
#           ex. Request has been added

class Admin(Person):

    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def setFirstName(self,firstName):
        self._firstName = firstName

    def setLastName(self, lastName):
        self._lastName = lastName

    def setEmail(self, email):
        self._email = email

    def setDepartment(self, department):
        self._email = email

    def getInfo(self):
        print(f'My name is {self._fistName} {self._lastName}. My email is {self._email}. I am working in {self._department} department.')

    def checkRequest(self):
        return('Request has been added')

    def addUser(self):
        return('User has been added')

    def addRequest(self):
        return('Request has been added')


    def getFullName(self):
        return(f"{self._firstName} {self._lastName}")

    def login(self):
        print(f'{self._email} has logged in')

    def logout(self):
        print(f'{self._email} has logged out')



# 5. Create a Request that has the following properties and methods
#   a. properties
#       name, requester, dateRequested, status
#   b. Methods (Don't forget to have getters and setters for the properties)
#       i. updateRequest() - returns "Request <name> has been updated"
#       ii. closeRequest() - returns "Request <name> has been closed"
#       iii. cancelRequest() - returns "Request <name> has been cancelled"
#       Note: All methods just return Strings of simple text
#           Ex. Request < name > has been updated/closed/cancelled

class Request():

    def __init__(self, name, requester, dateRequested):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = "Open"

    def setName(self,name):
        self._name = name

    def setRequester(self, requester):
        self._requester = requester

    def setDateRequested(self, dateRequested):
        self._dateRequested = dateRequested

    def set_status(self, status):
        self._status = status

    def getRequestInfo(self):
        print(f'{self._name} | {self._requester} | {self._dateRequested} | {self._status}')

    def updateRequest(self):
        return (f'Request {self._name} has been updated')

    def closeRequest(self):
        return (f'Request {self._name} has been closed')

    def cancelRequest(self):
        return (f'Request {self._name} has been cancelled')


# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())
